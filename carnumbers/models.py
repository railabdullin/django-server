from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone
from profiles.models import *

# Create your models here.
class CarNumber(models.Model):
  profile = models.ForeignKey(Profile , blank = True, null=True, on_delete=models.SET_NULL)

  searchable = models.BooleanField('Доступен для поиска', default=True)
  alarmEnabled = models.BooleanField('Уведомления включены', default=True)
  is_main_transport = models.BooleanField('Является приоритетным транспортом', default=True)

  adding_date = models.DateTimeField('Дата добавления', default=timezone.now)
  number = models.CharField("номер авто и регион" ,max_length=100, blank = True, null=True)
  hex_color = models.CharField("цвет авто" ,max_length=100, blank = True, null=True)

  locale_code_types = [
                        ('ru', ('Россия')),
                        ('us', ('США')),
    ]

  locale_code = models.CharField("буквенный код страны (ru, us и тп)" ,choices = locale_code_types, default='ru', max_length=100, blank = True, null=True)
  '''
  def create(self):
      request = self.context.get('request')
      print(request)
      user = request.user
      print(user)
      self.profile = Profile.objects.get(user = user)
  '''
  '''
  def save(self, *args, **kwargs):
      me = self.request.user
      print("me",me)
      user_profile = get_object_or_404(Profile.objects.all(),user=me)
      self.profile = user_profile
      super(CreateView, self).save()
  '''

  def __str__(self):
   return str(self.number)
