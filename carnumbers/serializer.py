from rest_framework import serializers
from .models import *
#from django.apps import apps

class CarNumberDetailSerializer(serializers.ModelSerializer):
    searchable = serializers.BooleanField(default=True, initial=True)
    alarmEnabled = serializers.BooleanField(default=True, initial=True)
    class Meta:
        #model = apps.get_model('carnumbers', 'CarNumber')
        model = CarNumber 
        fields = ('id', 'number', 'hex_color',  'locale_code', 'is_main_transport', 'searchable', 'alarmEnabled')

class ListSerializer(serializers.ModelSerializer):
  class Meta:
    model = CarNumber 
    #model = apps.get_model('carnumbers', 'CarNumber')
    fields =  ('id', 'number', 'hex_color',  'locale_code', 'is_main_transport', 'searchable', 'alarmEnabled')



class PostAdSerializer(serializers.ModelSerializer):
    searchable = serializers.BooleanField(default=True, initial=True)
    alarmEnabled = serializers.BooleanField(default=True, initial=True)


    class Meta:
        #model = apps.get_model('carnumbers', 'CarNumber')
        model = CarNumber 
        fields = ( 'profile', 'number', 'hex_color',  'locale_code', 'is_main_transport', 'searchable', 'alarmEnabled')

class CarNumberFoAPIClientSerializer(serializers.ModelSerializer):
    #searchable = serializers.BooleanField(default=True, initial=True)
    #alarmEnabled = serializers.BooleanField(default=True, initial=True)


    class Meta:
        model = CarNumber 
        #model = apps.get_model('carnumbers', 'CarNumber')
        fields = ( 'number',)
