from django.urls import path
from .views import *
from . import views

urlpatterns = [
  path('carnumbers/create/', CreateView.as_view()),
  path('carnumbers/search/', SearchListView.as_view()),
  path('carnumbers/', ListView.as_view()),
  path('carnumbers/<int:pk>', DeleteView.as_view()),
]
