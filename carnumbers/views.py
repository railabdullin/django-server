from django.shortcuts import render
from rest_framework import generics
from .serializer import *
from .models import *
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from django.db.models import Q
import urllib.parse

# Create your views here.
class CreateView(generics.CreateAPIView):
    serializer_class = CarNumberDetailSerializer

    def post(self, request):
        carnumber = request.POST.get('number', None)
        color = request.POST.get('color', None)
        locale_code = request.POST.get('locale_code', None)


        if carnumber is None and color is None and locale_code is not None:
            return Response({"message": "Bad request. Unexpected required params"}, status=400)

        me = self.request.user
        user_profile = get_object_or_404(Profile.objects.all(),user=me)
        #проверяем сколько у него авто
        cars_count = CarNumber.objects.filter(profile = user_profile).count()

        if cars_count >=3:
            return Response({"message": "Cars limit: 3"}, status=405)

        is_main_transport = False
        if cars_count == 0:
            is_main_transport = True
        else:
            main_cars_count = CarNumber.objects.filter(profile = user_profile, is_main_transport=True).count()
            if main_cars_count == 0:
                is_main_transport = True


        data = request.data.copy()
        data['profile'] = user_profile.id
        data['is_main_transport'] = is_main_transport


        serializer = PostAdSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            ad_saved = serializer.save()
        return Response(serializer.data)




''' Поиск номеров '''
class SearchListView(generics.ListAPIView):
  serializer_class = ListSerializer
  queryset = CarNumber.objects.all()

  def get(self, request, format=None, *args, **kwargs):
      if request.GET.get('carnumber'):
          carnumber = request.GET.get('carnumber')

          #carnumber = urllib.urlencode(carnumber)

          locale_code = request.GET.get('locale_code')
          print("carnumber '{}'".format(carnumber))
          instance = CarNumber.objects.filter(number = carnumber, locale_code=locale_code, searchable=True)

          serializer = ListSerializer(instance, many=True)
          return Response(serializer.data)

      else:
          return Response({"message": "Bad request"}, status=400)

''' Мои номера '''
class ListView(generics.ListAPIView):
  serializer_class = ListSerializer
  queryset = CarNumber.objects.all()

  def get(self, request, format=None, *args, **kwargs):
      print(request.user)
      user_profile = get_object_or_404(Profile.objects.all(),user=request.user)

      instance = CarNumber.objects.filter(profile = user_profile, searchable=True)

      serializer = ListSerializer(instance, many=True)
      return Response(serializer.data)


class DeleteView(generics.RetrieveUpdateDestroyAPIView):
  serializer_class = CarNumberDetailSerializer
  queryset = CarNumber.objects.all()

  def put(self, request, pk):
        user_profile = get_object_or_404(Profile.objects.all(),user=request.user)
        my_car_numbers = CarNumber.objects.filter(profile = user_profile)

        object = get_object_or_404(CarNumber.objects.all(), pk=pk)

        data = request.data.copy()

        #делаем так, чтобы был только один основной транспорт
        print(data['is_main_transport'])
        if data['is_main_transport'] == True or data['is_main_transport'] == 1 or data['is_main_transport'] == '1' :
            print("true")
            CarNumber.objects.filter(profile = user_profile).update(is_main_transport=False)
        else:
            #если до этого на этом авто был  is_main_transport = True, то ставим True другой машине
            if object.is_main_transport == True:
                next_carnumber = CarNumber.objects.filter(~Q(id = object.id), profile = user_profile)[:1][0]
                next_carnumber.is_main_transport=True
                next_carnumber.save()

        serializer = CarNumberDetailSerializer(instance=object, data=data, partial=True)
        if serializer.is_valid(raise_exception=True):
            me_saved = serializer.save()
        return Response({
            "success": "Updated successfully"
        })

  def delete(self, request, pk):
        # Get object with this pk
        item = get_object_or_404(CarNumber.objects.all(), pk=pk)
        user_profile = Profile.objects.get(user = self.request.user)

        if (item.profile == user_profile):
           item.delete()
           return Response(status=204)
        elif (item.profile != user_profile):
           return Response({"message": "No rights"}, status=405)
