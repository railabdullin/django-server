from django.contrib import admin

# Register your models here.
from .models import *

class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    list_display = ('number', 'profile', 'adding_date')

admin.site.register(CarNumber, ProfileAdmin)
