from django.contrib import admin
#from rest_framework.authtoken.admin import TokenAdmin

# Register your models here.
from .models import *

class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    list_display = ('user', 'cash', 'autobalance', 'parent_invoice', 'signup_date', 'comment')

class InvoiceAdmin(admin.ModelAdmin):
    model = Invoice
    list_display = ('profile', 'description', 'summ', 'status', 'create')

admin.site.register(Profile, ProfileAdmin)
#admin.site.register(TokenAdmin)
admin.site.register(Invoice, InvoiceAdmin)
