from datetime import datetime, timedelta
from rest_framework import serializers
from django.contrib.auth.models import User
from .models import *
from carnumbers.serializer import *
from carnumbers.models import *
from django.apps import apps
from connects.models import *

class DetailSerializer(serializers.ModelSerializer):
  class Meta:
    model = Profile
    fields = ('id', 'cash', 'blocked', 'block_description', 'autobalance')

class ListSerializer(serializers.ModelSerializer):
  class Meta:
    model = Profile
    fields = '__all__'

class PhoneUpdateSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ('username',)

class ReceiptSerializer(serializers.ModelSerializer):
  class Meta:
    model = Invoice
    fields = ('profile', 'description', 'parent_invoice', 'summ', 'slug',  'parking_session_id')

class CreditCardSerializer(serializers.ModelSerializer):
  class Meta:
    model = Invoice
    fields = ('profile', 'description', 'summ', 'slug')

class UserDataSerializer(serializers.ModelSerializer):
  vehicles = serializers.SerializerMethodField()
  cash = serializers.SerializerMethodField(read_only=True)
  #autobalance = serializers.SerializerMethodField()
  active_connect_id = serializers.SerializerMethodField(read_only=True)

  class Meta:
    model = Profile
    fields = ('id', 'cash', 'blocked', 'block_description', 'vehicles', 'credit_card_mask','slug','autobalance', 'active_connect_id', 'notification_device_token', 'parent_invoice')

  def get_vehicles(self, object):
    #model = apps.get_model('carnumbers', 'CarNumber')

    objects = CarNumber.objects.filter(profile=object)
    serializer = CarNumberDetailSerializer(objects, many = True)
    return serializer.data


  def get_cash(self, object):
    return object.cash / 100

  def get_autobalance(self, object):
    return object.autobalance / 100

  def get_active_connect_id(self, object):
    time_threshold = datetime.now() - timedelta(hours=2)
    count = Connect.objects.filter(author_profile=object, start_time__gte=time_threshold, finished=False).count()
    if count > 0:

      connect_id = Connect.objects.filter(author_profile=object, start_time__gte=time_threshold, finished=False)[:1][0].id
    else:
      connect_id = None

    return connect_id

  def update(self, instance, validated_data):
    instance.autobalance = validated_data.get('autobalance', instance.autobalance ) * 100
    instance.save()
    return instance


class NotificationTokenSerializer(serializers.ModelSerializer):
  class Meta:
    model = Profile
    fields = ('notification_device_token', )
