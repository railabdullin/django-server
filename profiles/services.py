from profiles.models import *
import logging

logger = logging.getLogger(__name__)

def check_and_make_autobalance_router(profile_id):
    profile = Profile.objects.get(id=profile_id)
    profile.check_and_make_autobalance()
