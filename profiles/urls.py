from django.urls import path
from .views import *
from . import views
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
  path('profiles/create/', CreateView.as_view()),
  path('profiles/', ListView.as_view()),
  path('profiles/<int:pk>', DeleteView.as_view()),

  path('token-auth/', ParkingLoginView.as_view(), name='knox_login'),
  path('app-auth/', csrf_exempt(UserLoginView.as_view()), name='knox_login'),
  path('send_code/', csrf_exempt(views.send_code), name='send_code'),
  path('my_user_data/', MyUserDataView.as_view(), name='send_code'),
  path('update_notification_token/',NotificationTokenUpdateView.as_view()),
  path('send_code_for_phone_change/', csrf_exempt(views.send_code_for_phone_change), name='send_code'),

  path('update_phone/', PhoneUpdateView.as_view(), name='send_code'),
  path('profiles/add_card/', CreditCardAddView.as_view()),
  path('card/register/start/', CardRegisterStart.as_view()),
  path('card/remove/', CardRemoveView.as_view()),
  #path('logout/',Logout.as_view()),
]
