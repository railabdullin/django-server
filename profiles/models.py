from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
#from .serializer import *
import random
import string
from hashlib import md5
#from parking.models import *
from django.apps import apps
from django_q.models import Schedule
import struct

from django.conf import settings

import logging
logger = logging.getLogger(__name__)

def double_to_hex(f):
    return hex(struct.unpack('<Q', struct.pack('<d', f))[0])

# Create your models here.
class Profile(models.Model):
    user = models.ForeignKey(User,related_name="profile", blank = True, null=True, on_delete=models.CASCADE)
    blocked = models.BooleanField('Заблокирован или нет', default=False)
    block_description = models.TextField('Описание причины блокировки', blank = True,  null=True)
    signup_date = models.DateTimeField('Дата регистрации', default=timezone.now)
    notification_device_token = models.CharField("токен от Firebase Cloud Messaging" ,max_length=1000, blank = True, null=True)
    code_for_phone_change = models.CharField("Код подтверждения для смены номера телефона" ,max_length=10, blank = True, null=True)
    cash = models.IntegerField("Баланс в копейках", default=0)
    slug = models.CharField("slug" ,max_length=60, blank = True, null=True, unique=True)
    payment_card_id = models.CharField("ID банковской карты" ,max_length=200, blank = True, null=True)
    parent_invoice = models.ForeignKey("Invoice", related_name="rel_parent_invoice", blank = True, null=True, on_delete=models.SET_NULL)
    autobalance = models.IntegerField("Сумма автопополнения в копейках", default=35000)
    credit_card_mask = models.CharField("Маска банковской карты" ,max_length=200, blank = True, null=True )
    comment = models.TextField('Комментарий', blank = True,  null=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=50)) # WARNING: Проверка на уникальность
        super(Profile, self).save(*args, **kwargs)

    def __str__(self):
        return self.user.username

    def check_and_make_autobalance(self):
        logger.info("Проверяем баланс для автопополнения. Баланс {}, автопополнение: {}".format(self.cash, self.autobalance))
        if self.cash < self.autobalance:
            summ = self.autobalance - self.cash
            logger.info("Сумма автопополнения: {}".format(summ))
            if summ <= 0:
                response = {"status": "success" }
            parent_invoice = self.parent_invoice    
            data = {
                'profile' : self.id,
                'description' : "Автопополнение баланса на  {} руб.".format(summ/100),
                'summ' : summ ,
                'parent_invoice' : parent_invoice.id
                            }
            #serializer = ReceiptSerializer(data=data)
            #if serializer.is_valid(raise_exception=True):
            #    invoice = serializer.save()
            invoice = apps.get_model('profiles', 'Invoice')()
            invoice.profile = self
            invoice.description = "Автопополнение баланса на  {} руб.".format(summ/100)
            invoice.summ = summ
            invoice.parent_invoice = parent_invoice
            invoice.save()
            schedule = Schedule.objects.create(
                                name="Make Autobalance (Recurrent)",
                                func="parking.services.make_payment",
                                args=f"{invoice.id,}",
                            )
        

class Invoice(models.Model):
    profile = models.ForeignKey(Profile,related_name="_profile", blank = True, null=True, on_delete=models.SET_NULL)
    parent_invoice = models.ForeignKey('self', blank = True, null=True, on_delete=models.SET_NULL)
    description = models.TextField('Описание платежа', blank = True,  null=True)
    create = models.DateTimeField('Дата и время проведения', default=timezone.now)
    summ = models.IntegerField("Сумма платежа в копейках", default=0)
    #parking_session = models.ForeignKey("ParkingSession" ,related_name="session", blank = True, null=True, on_delete=models.SET_NULL)
    parking_session_id = models.IntegerField("Id сенанса парковки", default=None, blank = True, null=True)
    type_choice = [('process', ('Проводится')), ('finish', ('Успешно завершен')), ('error', ('Завершен с ошибкой')),]
    status = models.CharField("Статус платежа" ,choices = type_choice, default='process', max_length=10, blank = True, null=True)
    slug = models.CharField("slug" ,max_length=60, blank = True, null=True, unique=True)
    payment_request_response = models.TextField('Ответ эквайера на запрос о списании', blank = True,  null=True)

    def __str__(self):
        return "{}, ID сеанса: {},  ID счета: {}".format(self.description, self.parking_session_id, self.id)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=50))  # WARNING: Проверка на уникальность
        super(Invoice, self).save(*args, **kwargs)

    def get_signature(self, is_recurrent=False):
        password = settings.UNITELLER_PASSWORD
        Subtotal_P = self.summ / 100
        Order_IDP = self.slug
        Shop_IDP = settings.UNITELLER_SHOP_IDP

        Customer_IDP = self.profile.slug
        Lifetime = "3600"
        Parent_Order_IDP = self.parent_invoice.slug

        print("Parent_Order_IDP ", Parent_Order_IDP)
        print("Order_IDP ", Order_IDP)
        print("Subtotal_P ", Subtotal_P)

        print("password ", password)

        Shop_IDP_md5 = md5(Shop_IDP.encode(encoding = 'ascii')).hexdigest()
        Order_IDP_md5 = md5(Order_IDP.encode(encoding = 'ascii')).hexdigest()
        Lifetime_md5 = md5("3600".encode(encoding = 'ascii')).hexdigest()
        Customer_IDP_md5 = md5(Customer_IDP.encode(encoding = 'ascii')).hexdigest()
        password_md5 = md5(password.encode(encoding = 'ascii')).hexdigest()
        ap = "&" #.encode(encoding = 'ascii') #.encode(encoding = 'ascii')
        Parent_Order_IDP_md5 = md5(Parent_Order_IDP.encode(encoding = 'ascii')).hexdigest()
        Subtotal_P_md5 = md5(str(Subtotal_P).encode(encoding = 'ascii')).hexdigest()
        print("type ", type(Subtotal_P))
        #doubleAsHex = double_to_hex(Subtotal_P)
        #doubleAsBytes = bytearray.fromhex(doubleAsHex.replace('0x',''))


        #Subtotal_P_md5 = md5(doubleAsBytes).hexdigest()
        if is_recurrent:
            code = str(Shop_IDP_md5) + ap + str(Order_IDP_md5) + ap + str(Subtotal_P_md5) + ap + str(Parent_Order_IDP_md5) + ap + str(password_md5)
        else:
            code = str(Shop_IDP_md5) + ap + str(Order_IDP_md5) + ap + str(Lifetime_md5) + ap + str(Customer_IDP_md5) + ap + str(password_md5)

        print(code)

        signature = md5((code).encode(encoding = 'ascii') ).hexdigest().upper()
        return signature
