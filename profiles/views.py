from django.shortcuts import render
from rest_framework import generics
from .serializer import *
from .models import *
from parking.models import *
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model, logout

from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rest_framework.response import Response
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.generics import RetrieveUpdateDestroyAPIView, UpdateAPIView
from rest_framework import permissions
from rest_framework.generics import get_object_or_404
from django.views.generic import TemplateView
from django.views import View
from rest_framework.views import APIView
from rest_framework import status

from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
import random, string

from phonenumber_field.phonenumber import PhoneNumber
from hashlib import md5
import requests
from requests.exceptions import HTTPError
#import phonenumbers
from django.conf import settings

import logging
logger = logging.getLogger(__name__)


''' ОТПРАВКА СМС '''
import urllib
import base64
import sys
import urllib.request


smsUsername = 'z1565355522515'
smsPassword = '551269'

class Gate:
    """class for using iqsms.ru service via GET requests"""

    __host = 'gate.iqsms.ru'

    def __init__(self, api_login, api_password):
        self.login = api_login
        self.password = api_password

    def __sendRequest(self, uri, params=None):
        url = self.__getUrl(uri, params)
        request = urllib.request.Request(url)
        passman = urllib.request.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, url, self.login, self.password)
        authhandler = urllib.request.HTTPBasicAuthHandler(passman)
        try:
            opener = urllib.request.build_opener(authhandler)
            data = opener.open(request).read()
            return data
        except e:
            return e.code

    def __getUrl(self, uri, params=None):
        url = "https://%s/%s/" % (self.getHost(), uri)
        paramStr = ''
        if params is not None:

            paramStr = urllib.parse.urlencode(params)
            print(paramStr)
        return "%s?%s" % (url, paramStr)

    def getHost(self):
        """Return current requests host """
        return self.__host

    def setHost(self, host):
        """Changing default requests host """
        self.__host = host

    def send(self, phone, text, sender='iqsms',
             statusQueueName=None, scheduleTime=None, wapurl = None):
        """Sending sms """
        params = {'phone': phone,
                  'text': text,
                  'sender': sender,
                  'statusQueueName': statusQueueName,
                  'scheduleTime': scheduleTime,
                  'wapurl': wapurl
                 }
        return self.__sendRequest('send', params)

    def status(self, id):
        """Retrieve sms status by it's id """
        params = {'id': id}
        return self.__sendRequest('status', params)

    def statusQueue(self, statusQueueName, limit = 5):
        """Retrieve latest statuses from queue """
        params = {'statusQueueName': statusQueueName, 'limit': limit}
        return self.__sendRequest('statusQueue', params)

    def credits(self):
        """Retrieve current credit balance """
        return self.__sendRequest('credits')

    def senders(self):
        """Retrieve available signs """
        return self.__sendRequest('senders')



if __name__ == "__main__":
    print (Gate.__doc__)


''' Конец отправка смс'''


class NotificationTokenUpdateView(UpdateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = NotificationTokenSerializer
    
    def get_object(self):
        object = get_object_or_404(Profile.objects.all(), user=self.request.user)
        return object

class MyUserDataView(RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.IsAuthenticated]

    serializer_class = UserDataSerializer
    def get_object(self):
        object = get_object_or_404(Profile.objects.all(), user=self.request.user)
        print(f"parent_invoice {object.parent_invoice}")        

        # проверяем автобаланс: перенести чтобы сразу при регистрации карты делал списание
        if object.parent_invoice is not None:
            object.check_and_make_autobalance()

        if object.credit_card_mask is None and object.parent_invoice is not None:
            url = "https://wpay.uniteller.ru/cardv3/"
    
            password = settings.UNITELLER_PASSWORD 
            Shop_IDP = settings.UNITELLER_SHOP_IDP
            Login = settings.UNITELLER_LOGIN

            params = {
             "Shop_IDP" : Shop_IDP,
             "Login" : Login,
             "Password" : password,
             "Customer_IDP" : object.slug
            }

            try:
                response = requests.post(url, data=params)

                # если ответ успешен, исключения задействованы не будут
                response.raise_for_status()

                csv_response = response.text
                print("Ответ от Uniteller на запрос о реккурентном платеже: {}".format(csv_response))
                print("csv_response: ",csv_response)

                array = []
                for line in csv_response.split("\n"):
                    row = []
                    if line == "" or  line == " ":
                        continue
                    for item in line.split(";"):
                        row.append(item)
                    array.append(row)

                dict = {array[0][i]:array[len(array)-1][i] for i in range(len(array[0]))}
                print("dict", dict)

                if not "ErrorCode" in dict:
                    if  "CardNumber" in dict:
                        mask = dict["CardNumber"]
                        if mask == "CardNumber":
                            mask = None
                            object.parent_invoice = None
                        object.credit_card_mask = mask
                        object.save()

            except HTTPError as http_err:
                print(f'HTTP error occurred: {http_err}')  # Python 3.6
            except Exception as err:
                print(f'Other error occurred: {err}')  # Python 3.6
            else:
                print('Success! Payment')


        #переводим сумму автобаланса из копеек в рубли
        object.autobalance = object.autobalance / 100
        return object #get_object_or_404(Profile.objects.all(), user=self.request.user)


# Create your views here.
class CreateView(generics.CreateAPIView):
    serializer_class = DetailSerializer

class ListView(generics.ListAPIView):
    serializer_class = ListSerializer
    queryset = Profile.objects.all()

class DeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DetailSerializer
    queryset = Profile.objects.all()


''' Смена номера телефона '''


@api_view(('POST',))
@renderer_classes((JSONRenderer,))
@permission_classes((AllowAny, ))
def send_code_for_phone_change(request):
    if request.method == 'POST':
        phone = request.POST.get('phone', None)

        if phone is not None:

            #валидация номера телефона
            phone = PhoneNumber.from_string(phone_number=phone, region='RU').as_e164

            #код подтверждения записываем в пароль
            code = ''.join(random.choice(string.digits) for _ in range(4))

            #проверяем нет ли пользователей с таким номеров
            users_count = User.objects.filter(username=phone).count()

            if users_count == 0:
                user = request.user
                profile = get_object_or_404(Profile.objects.all(), user=user)
                profile.code_for_phone_change = code
                profile.save()

                #отправляем смс код на новый телефона
                sender = Gate(smsUsername, smsPassword);

                print (sender.send(phone, 'You code for Numpass {}'.format(code), 'Numpass')) #отправляем sms
                return Response({"message": "OK"}, status=200)
            else:
                return Response({"message": "This phone already uses"}, status=405)

        else:
            return Response({"message": "Bad request. Unexpected required params"}, status=400)
    else:
        return Response({"message": "Not allowed"}, status=405)

class PhoneUpdateView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PhoneUpdateSerializer
    queryset = User.objects.all()


    def put(self, request):
        user = request.user
        data = request.data.copy()

        data['username'] = PhoneNumber.from_string(phone_number=data['phone'], region='RU').as_e164

        profile = get_object_or_404(Profile.objects.all(), user=user)

        code_for_phone_change = profile.code_for_phone_change

        #проверяем верно ли введен код
        code = data['code']

        if code == code_for_phone_change:
            serializer = PhoneUpdateSerializer(instance=user, data=data, partial=True)
            if serializer.is_valid(raise_exception=True):
                me_saved = serializer.save()
            return Response({"success": "Updated successfully"})
        else:
            return Response({"success": "Code is not valid"}, status=405)




from django.contrib.auth import login

from rest_framework import permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView

class UserLoginView(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        data = request.data.copy()
        phone = data["username"]

        #валидация номера телефона
        phone = PhoneNumber.from_string(phone_number=phone, region='RU').as_e164
        data["username"] = phone
        print(data)
        serializer = AuthTokenSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        print("dds")
        '''
        #определяем клиента api
        users_count = ParkingClient.objects.filter(user=user).count()
        print("users_count",users_count)
        if users_count == 0 :
            login(request, user)
            return super(UserLoginView, self).post(request, format=None)
            #return Response({"message": "You are not our API client"}, status=205)
        elif users_count > 1:
            parking_client = ParkingClient.objects.filter(user=user)[:1][0]
            parking_client_profile = Profile.objects.filter(user=user)[:1][0]
        else:
            parking_client = ParkingClient.objects.get(user=user)
            parking_client_profile = Profile.objects.get(user=user)

        #если это api client и у него не было до этого циклической задачи на проверку запросов, то создаем циклическую задачу, чтобы каждые 10 минут делать запрос на проверку номеров
        if parking_client.schedule is None:
            # Create the schedule
            schedule = Schedule.objects.create(
                name=self.__str__(),
                func="parking.services.check_balance_of_many_numbers",
                args=f"{parking_client.id}",
                schedule_type=Schedule.MINUTES,
            )
            parking_client.schedule = schedule
            parking_client.save()
        '''
        login(request, user)
        return super(UserLoginView, self).post(request, format=None)

class ParkingLoginView(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        data = request.data.copy()
        '''
        phone = data["username"]

        #валидация номера телефона
        print( PhoneNumber.is_valid(phone))
        phone = PhoneNumber.from_string(phone_number=phone, region='RU').as_e164
        data["username"] = phone
        print(data)
        '''
        logger.info("Попытка авторизации: {}".format(data))
        serializer = AuthTokenSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        logger.info("Успешная авторизация: {}".format(user))
        #определяем клиента api
        users_count = ParkingClient.objects.filter(user=user).count()
        print("users_count",users_count)
        if users_count == 0 :
            login(request, user)
            return super(ParkingLoginView, self).post(request, format=None)
            #return Response({"message": "You are not our API client"}, status=205)
        elif users_count > 1:
            parking_client = ParkingClient.objects.filter(user=user)[:1][0]
            parking_client_profile = Profile.objects.filter(user=user)[:1][0]
        else:
            parking_client = ParkingClient.objects.get(user=user)
            parking_client_profile = Profile.objects.get(user=user)
        '''
        #если это api client, у него выбран тип получения цен: запросы на его сервер и у него не было до этого циклической задачи на проверку запросов, то создаем циклическую задачу, чтобы каждые 10 минут делать запрос на проверку номеров
        if parking_client.schedule is None and parking_client.get_price_method == "recieve":
            # Create the schedule
            schedule = Schedule.objects.create(
                name=self.__str__(),
                func="parking.services.check_balance_of_many_numbers",
                args=f"{parking_client.id}",
                schedule_type=Schedule.MINUTES,
            )
            logger.info("Создан schedule : {}".format(schedule))
            parking_client.schedule = schedule
            parking_client.save()
        '''
        login(request, user)
        return super(ParkingLoginView, self).post(request, format=None)

@api_view(('POST',))
@renderer_classes((JSONRenderer,))
@permission_classes((AllowAny, ))
def send_code(request):
    if request.method == 'POST':
        phone = request.POST.get('phone', None)

        if phone is not None:

            #валидация номера телефона
            phone = PhoneNumber.from_string(phone_number=phone, region='RU').as_e164

            #код подтверждения записываем в пароль
            new_password = ''.join(random.choice(string.digits) for _ in range(4))

            #проверяем нет ли пользователей с таким номеров
            users_count = User.objects.filter(username=phone).count()
            
         

            if phone == "+79961028345" or phone == "+79106625728":
                new_password = "1111"

            # Для тестовой регистрации большого количества номеров
            print(f"phone[:3]= {phone[:3]}")
            if phone[:3] == "+11":
                new_password = "1111"
  
            if users_count == 0:
                new_user = User()
                new_user.username = phone
                new_user.password = make_password(new_password)
                new_user.save()

                new_profile = Profile()
                new_profile.user = new_user
                new_profile.save()

            else:
                user = User.objects.filter(username=phone)[:1][0]
                user.password = make_password(new_password)
                user.save()

            #отправляем смс код
            sender = Gate(smsUsername, smsPassword);
            if phone != "+79961028345" and phone != "+79106625728" and phone[:3] != "+11":
                print (sender.send(phone, 'You code for Numpass {}'.format(new_password), 'Numpass')) #отправляем sms
            return Response({"message": "OK"}, status=200)

        else:
            return Response({"message": "Bad request. Unexpected required params"}, status=400)
    else:
        return Response({"message": "Not allowed"}, status=405)



class CreditCardAddView(generics.CreateAPIView):
    serializer_class = CreditCardSerializer

    def post(self, request):
        profile = Profile.objects.get(user=request.user)
        profile_id = profile.id
        data = {
            "profile" : profile_id,
            "description" : "Сохранение карты",
            "summ" : 1000,
        }

        serializer = CreditCardSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_invoice = serializer.save()

        url = "https://wpay.uniteller.ru/pay/"

        password = "p73VCKfK9zpqoEbuTQU50cclryYG9paX8mmBaCFYYjV1HRt45ojjcZ4ejvFYG7OjfzUBbTvTIoOGnmzC" #WARNING: вывести в отдельный файл
        Subtotal_P = new_invoice.summ / 100
        Order_IDP = new_invoice.slug
        Shop_IDP = "00023365" #WARNING: вывести в отдельный файл
        Customer_IDP = profile.slug
        '''
        Shop_IDP_md5 = md5(Shop_IDP.encode()).hexdigest().encode()
        Order_IDP_md5 = md5(Order_IDP.encode()).hexdigest().encode()
        Lifetime_md5 = md5("300".encode()).hexdigest().encode()
        Customer_IDP_md5 = md5(Customer_IDP.encode()).hexdigest().encode()
        password_md5 = md5(password.encode()).hexdigest().encode()
        

        Shop_IDP_md5 = md5(Shop_IDP.encode(encoding = 'ascii'))
        Order_IDP_md5 = md5(Order_IDP.encode(encoding = 'ascii'))
        Lifetime_md5 = md5("300".encode(encoding = 'ascii'))
        Customer_IDP_md5 = md5(Customer_IDP.encode(encoding = 'ascii'))
        password_md5 = md5(password.encode(encoding = 'ascii'))
        ap = "&".encode(encoding = 'ascii')

        signature = md5( Shop_IDP_md5 + ap + Order_IDP_md5 + ap + Lifetime_md5 + ap + Customer_IDP_md5 + ap + password_md5 ).hexdigest().uppercase()
        '''
        signature = new_invoice.get_signature()
        params = {
        "Shop_IDP" : Shop_IDP,
        "Order_IDP" : Order_IDP,
        "Subtotal_P" : Subtotal_P,
        "Signature" : signature,
        "IsRecurrentStart" : "1",
        }

        print("make_payment")
        print(new_invoice)

        form_url = None
        try:
            response = requests.post(url, data=params)

            # если ответ успешен, исключения задействованы не будут
            response.raise_for_status()

            csv_response = response.text #()
            print("response = ", csv_response)
            reader = csv.reader(csv_response.iter_lines(), delimiter=';', quotechar='"')
            for row in reader:
                print (row)

        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')  # Python 3.6
        except Exception as err:
            print(f'Other error occurred: {err}')  # Python 3.6
        else:
            print('Success! Payment')

        response = {"form_url" : form_url}

        return Response(response)



class CardRegisterStart(TemplateView):
    template_name = "banking/card_register.html"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        if self.request.user.id is None:
            uid = self.request.GET.get("uid", None)
            profile = get_object_or_404(Profile, slug=uid)
        else:
            profile = Profile.objects.get(user=self.request.user)
        
        if profile is None:
            return Response("error 0")

        #profile = Profile.objects.get(user=self.request.user)
        profile_id = profile.id
        data = {
            "profile" : profile_id,
            "description" : "Сохранение карты",
            "summ" : 1000,
        }

        serializer = CreditCardSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_invoice = serializer.save()

        password = settings.UNITELLER_PASSWORD  #"p73VCKfK9zpqoEbuTQU50cclryYG9paX8mmBaCFYYjV1HRt45ojjcZ4ejvFYG7OjfzUBbTvTIoOGnmzC" #WARNING: вывести в отдельный файл
        Subtotal_P = new_invoice.summ / 100
        Order_IDP = new_invoice.slug
        Shop_IDP = settings.UNITELLER_SHOP_IDP #"00014165" #WARNING: вывести в отдельный файл
        Customer_IDP = profile.slug
        Lifetime = "3600"
  
        profile.parent_invoice = new_invoice
        profile.save()
 
        # СОздаем Scheduler на вызов автополонения после регистрации карты
        '''
        schedule = Schedule.objects.create(
                name=self.__str__(),
                func="parking.services.check_balance_of_many_numbers",
                args=f"{parking_client.id}",
                schedule_type=Schedule.MINUTES,
            )
        '''
        """
        Shop_IDP_md5 = md5(Shop_IDP.encode()).hexdigest().encode()
        Order_IDP_md5 = md5(Order_IDP.encode()).hexdigest().encode()
        Lifetime_md5 = md5("3600".encode()).hexdigest().encode()
        Customer_IDP_md5 = md5(Customer_IDP.encode()).hexdigest().encode()
        password_md5 = md5(password.encode()).hexdigest().encode()
        """
        Shop_IDP_md5 = md5(Shop_IDP.encode(encoding = 'ascii')).hexdigest()
        Order_IDP_md5 = md5(Order_IDP.encode(encoding = 'ascii')).hexdigest()
        Lifetime_md5 = md5("3600".encode(encoding = 'ascii')).hexdigest()
        Customer_IDP_md5 = md5(Customer_IDP.encode(encoding = 'ascii')).hexdigest()
        password_md5 = md5(password.encode(encoding = 'ascii')).hexdigest()
        ap = "&" #.encode(encoding = 'ascii') #.encode(encoding = 'ascii')

        code = str(Shop_IDP_md5) + ap + str(Order_IDP_md5) + ap + str(Lifetime_md5) + ap + str(Customer_IDP_md5) + ap + str(password_md5)

        #signature = md5( Shop_IDP_md5 + b"&" + Order_IDP_md5 + b"&" + Lifetime_md5 + b"&" + Customer_IDP_md5 + b"&" + password_md5 ).hexdigest()
        signature = md5((code).encode(encoding = 'ascii') ).hexdigest().upper()
        context['Order_ID'] =  Order_IDP
        context['Shop_ID'] = Shop_IDP

        context['Customer_IDP'] = Customer_IDP
        context['Signature'] = signature
        return context

class CardRemoveView(APIView):
    def post(self, request, *args, **kwargs):
        profile = get_object_or_404(Profile.objects.all(), user=request.user)
        print(f"profile {profile}")
        profile.parent_invoice = None
        profile.credit_card_mask = None
        profile.save()
        
        return Response('ok')

class InvoicePaymentCallback(APIView):
    permission_classes = (permissions.AllowAny,)
    def post(self, request, *args, **kwargs):
        logger.info("Callback от Uniteller. Data: {}".format(request.data))
        print(request.data)
        signature = request.POST.get("Signature", None)
        order_id = request.POST.get("Order_ID", None)
        status = request.POST.get("Status", None)

        # Проверяем signature
        password = "p73VCKfK9zpqoEbuTQU50cclryYG9paX8mmBaCFYYjV1HRt45ojjcZ4ejvFYG7OjfzUBbTvTIoOGnmzC" #WARNING: вывести в отдельный файл
        raw_signature = order_id + status + password
        true_signature = md5(raw_signature.encode(encoding = 'ascii')).hexdigest().upper()
        
        if true_signature != signature:
            return Response('Invalid signature', status=400)

        if status == "paid":
            invoice = get_object_or_404(Invoice.objects.all(), slug=order_id)
            invoice.type = "finish"
            invoice.save()
            logger.info("Изменили статус счета")

        print(true_signature)
        return Response('ok')


class Logout(APIView):
    def get(self, request, format=None):
        # simply delete the token to force a login
        #        request.user.token.delete()
        logout(request)
        return Response(status=status.HTTP_200_OK)
