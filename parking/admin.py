from django.contrib import admin

# Register your models here.
from .models import *

from django_q.models import Schedule

class ParkingSessionAdmin(admin.ModelAdmin):
    model = ParkingSession
    list_display = ('carnumber', 'profile', 'begin_time', 'finish_time', 'finished', 'payed', 'has_payment_error')

admin.site.register(ParkingSession, ParkingSessionAdmin)
admin.site.register(ParkingClient)
