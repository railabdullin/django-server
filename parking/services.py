import requests
#from . import local_settings
from profiles.serializer import *
from profiles.models import *
from .models import *
from carnumbers.serializer import *
from requests.exceptions import HTTPError
import datetime
import json
from django.utils import timezone
import multiprocessing
from django_q.models import Schedule
from hashlib import md5
import csv
import logging
logger = logging.getLogger(__name__)
from django.conf import settings


def make_payment(invoice):
    """
    данная функция делает попытку списать стоимость за парковку с банковской карты пользователя, который идентифицируется по id карты в системе мерчанта (платежной системы uniteller).
    Если запрос прошел успешно, то баланс пополняется на определенную сумму.
    Если деньги списать не удалось, то отправляется push и смс уведомление о том, что карту или баланс нужно пополнить
    """
    if type(invoice) == int:
        try:
            invoice = Invoice.objects.get(id=invoice)
        except:
            return

    logger.info("parking.services.make_payment. Invoice: {}".format(invoice))

    url = "https://wpay.uniteller.ru/recurrent/"

    password = settings.UNITELLER_PASSWORD   # "p73VCKfK9zpqoEbuTQU50cclryYG9paX8mmBaCFYYjV1HRt45ojjcZ4ejvFYG7OjfzUBbTvTIoOGnmzC" #WARNING: вывести в отдельный файл
    Parent_Order_IDP = invoice.parent_invoice.slug
    Subtotal_P = invoice.summ / 100
    Order_IDP = invoice.slug
    Shop_IDP = settings.UNITELLER_SHOP_IDP  # "00014165"

    #signature = md5( Shop_IDP_md5 + b"&" + Order_IDP + b"&" + Lifetime + b"&" + Customer_IDP + b"&" + password ).hexdigest()
    Customer_IDP = invoice.profile.slug

    signature =  invoice.get_signature(is_recurrent=True)  #md5( Shop_IDP_md5 + b"&" + Order_IDP_md5 + b"&" + Lifetime_md5 + b"&" + Customer_IDP_md5 + b"&" + password_md5 ).hexdigest()
    params = {
        "Shop_IDP" : Shop_IDP,
        "Order_IDP" : Order_IDP,
        "Subtotal_P" : Subtotal_P,
        "Parent_Order_IDP" : Parent_Order_IDP,
        "Signature" : signature
    }

    session_id = invoice.parking_session_id
    session = None
    if session_id != None:
       sessions_count = ParkingSession.objects.filter(id=session_id).count()
       if sessions_count == 0 :
           print("Нет такой сессии")
           #WARNING: в логфайл вывести
       elif sessions_count > 1:
           session = ParkingSession.objects.filter(id=session_id)[:1][0]
       else:
           session = ParkingSession.objects.get(id=session_id)
    print(f"session ID: {session}")
    logger.info("session {}".format( session))
    print("Запрос на рекурентный платжен на сумму: ", Subtotal_P)

    try:
        response = requests.post(url, data=params)
        # если ответ успешен, исключения задействованы не будут
        response.raise_for_status()
        csv_response = response.text
        logger.info("Ответ от Uniteller на запрос о реккурентном платеже: {}".format(csv_response))
        print("csv_response: ",csv_response)
        invoice.payment_request_response = csv_response
        invoice.save()
        array = []
        for line in csv_response.split("\n"):
            row = []
            if line == "" or  line == " ":
                continue
            for item in line.split(";"):
                row.append(item)
            array.append(row)

        dict = {array[0][i]:array[1][i] for i in range(len(array))}

        if "ErrorCode" in dict:
            if dict["ErrorCode"] != "0":
                print("Ошибка списания средств")

                invoice.status = "error"
                invoice.save()

                #показывает push уведомелние что не выедет пока не оплатит
                message_title = session.carnumber
                message_body = "У вас есть долг. Возникли проблемы при оплате с банковской картой. Проверьте баланс"
                fcm_id = invoice.profile.notification_device_token
                if fcm_id != None:
                    result = push_service.notify_single_device(registration_id=fcm_id, message_title=message_title, message_body=message_body)
                    print (result)

                #WARNING: в логфайл вывести
                session.has_payment_error = True
                session.save()
                logger.info("Ошибка списания средств. Создаем Schedule")
                print("Ошибка списания средств. Создаем Schedule")
                schedule = Schedule.objects.create(
                            name="check_balance",
                            func="parking.services.check_balance",
                            args=f"{session.id, invoice.summ,}",
                        )
                print("schedule id: {}".format(schedule.id))
                logger.info("schedule id: {}".format(schedule.id))

        #если платеж удачен, то меняем родительский платеж этого человека на id текущего заказа, т.к. родитльский платжен должен быть новее чем полгода
        if "Response_Code" in dict:
            print("in dict")
            if dict["Response_Code"] == "AS000":
                 print("успешно списаны деньги")
                 invoice.status = "finish"
                 invoice.save()
                 #успешно списаны деньги
                 profile = invoice.profile
                 logger.info("Деньги успешно списаны. Прошлый баланс: {} р., сумма пополнения: {} р." .format(profile.cash/100, invoice.summ/100))
                 profile.parent_invoice  = invoice
                 profile.cash = profile.cash + invoice.summ
                 profile.save()
                 print("Деньги успешно списаны. Прошлый баланс: {} р., сумма пополнения: {} р." .format(profile.cash/100, invoice.summ/100))
                 '''
                 sessio_id = invoice.parking_session_id
                 if session_id != None:
                     sessions_count = ParkingSession.objects.filter(id=session_id).count()
                     if sessions_count == 0 :
                         print("Нет такой сессии")
                     elif sessions_count > 1:
                         session = ParkingSession.objects.filter(id=session_id)[:1][0]
                     else:
                         session = ParkingSession.objects.get(id=session_id)
                 '''
                 session.reccurent_schedules.add(schedule)
                 session.has_payment_error = False
                 session.save()
            else:
                print("Ошибка списания средств")
                logger.info("Ошибка списания средств. Создаем Schedule") 
                invoice.status = "error"
                invoice.save()
                #если счет создан для автополнения к сессии то показывает push уведомелние что не выедет пока не оплатит
                if session is not None:
                    message_title = session.carnumber
                    message_body = "У вас есть долг. Возникли проблемы при оплате с банковской картой. Проверьте баланс"
                    fcm_id = invoice.profile.notification_device_token
                    if fcm_id != None:
                        result = push_service.notify_single_device(registration_id=fcm_id, message_title=message_title, message_body=message_body)
                        print (result)

                print("Ошибка списания средств. Создаем Schedule")


                # Если сессия не пустая, то создаем задчу, которая будет проверять баланс и стоимость сессии.
                # Если сессия пустая, то создаем задачу на make_payment чтобы ппробовал еще раз списать деньги.
                # IDEA: в будущем сделать 5 попыток списания средств с нарастающим интервалом и если не удалось, то завершить попытки, чтобы не нагружать сервер
                # и платежный сервис
                if session is not None:
                    schedule = Schedule.objects.create(
                            name="check_balance",
                            func="parking.services.check_balance",
                            args=f"{session.id, invoice.summ,}",
                        )
                    session.reccurent_schedules.add(schedule)
                    session.has_payment_error = True
                    session.save()
                #else:
                #    # Если это оплата для проведения автополполнения платежа, то пока не создаем, чтобы минимизировать нагрузку на платежный сервис
                #    schedule = Schedule.objects.create(
                #            name="Make Autobalance (Recurrent)",
                #            func="parking.services.make_payment",
                #            args=f"{invoice.id,}",
                #        )
                print("schedule id: {}".format(schedule.id))

            #если пошла ошибка пополнения баланса, то показываем push, ставим отметку в сеансе парковки что проблема с оплатой
            # если оплата умпешна, то снимаем пометку ошибки в оплате


    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')  # Python 3.6
    except Exception as err:
        print(f'Other error occurred: {err}')  # Python 3.6
    else:
        print('Success request')



'''
def check_balance(session):
    """
    данная функция проверяет баланс пользователя каждые 30 минут и если баланс меньеш чем текущая стоимость за парковку + 1 час,
    то автоматический пополняет баланс, путем списывания денежных средств с карты пользователя.

    Текущая стоимость за парковку определяется выбранным при подключении клиента API вариантом:
    1-ый - делаем запрос клиенту на указанный url и получаем оттуда переменную price
    2-ой - считаем цену сами из тарифа
    """
    print("WOW ",type( session))


    if type(session) == str:
        print('got str ', session)
        session = ParkingSession.objects.get(id=session) # WARNING: Исключение если такой сессиии нет
        print('got str ', session)
    #получаем баланс пользователя
    balance = session.carnumber.profile.cash

    #определяем сколько времени находился человек на парковке с учетом часовых поясов, в минутах
    duration = 60

    #оперделяем стоимость парковки согласну варианту определяения цены
    if session.parking_client.get_price_method == 'request':
        request_url = session.parking_client.get_price_request_url

        try:
            response = requests.post(request_url, data={'number': session.carnumber.number })

            if response.status_code == 200:
                json = response.json
                price = json['price']
                print(price)
            elif response.status_code == 404:
                print('Not Found.')
                return

            response.raise_for_status()
        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')  # Python 3.6
            return
        except Exception as err:
            print(f'Other error occurred: {err}')  # Python 3.6
            return
        else:
            print('Success!')

    elif session.parking_client.get_price_method == 'calculate':
        pass

    #проверяем баланс
    if balance < price:
        #запрос на списывание денег с карты
        card_id = session.carnumber.profile.payment_card_id   #id карты в системе мерчанта
        summ = price - balance    #сумма которую нужно списать

        if card_id == None:
            print("no card")
            # WARNING: Себе в логи и БД запсать, чтобы этот пользователь больше не заезжал по картк

        if summ > 0:
            #создаем платеж
            parent_invoice = Invoice.objects.filter(profile=session.carnumber.profile, status='finish').latest('id')
            data = {
                'profile' : session.carnumber.profile.id,
                'description' : "Оплата {} руб. за парковку".format(summ),
                'summ' : summ,
                'parent_invoice' : parent_invoice
            }
            serializer = ReceiptSerializer(data=data)
            if scaning_serializer.is_valid(raise_exception=True):
                invoice = scaning_serializer.save()


            make_payment(invoice)
'''

def check_balance(session_id, price):
    """
    данная функция проверяет баланс пользователя и если баланс меньеш чем текущая стоимость за парковку ,
    то автоматический пополняет баланс, путем списывания денежных средств с карты пользователя.

    Текущая стоимость за парковку определяется выбранным при подключении клиента API вариантом:
    1-ый - делаем запрос клиенту на указанный url и получаем оттуда переменную price
    2-ой - считаем цену сами из тарифа
    """
    print("check_balance ", session_id)
    logger.info("Проверяем баланс для сессии парковки с id: {}".format(session_id))

    #получаем баланс пользователя
    try:
        session = ParkingSession.objects.get(id=session_id)
    except:
        return
        pass
    balance = session.carnumber.profile.cash

    #если включен тестовый режим то не доходим до списания средств
    if session.parking_client.test_mode == True:
        logger.info("Включен тестовый режим. Выходим из функции")
        return

    logger.info("проверяем баланс balance: {}, price: {}".format(balance, price))
    #проверяем баланс
    if balance < price:
        #запрос на списывание денег с карты
        card_id = session.carnumber.profile.payment_card_id   #id карты в системе мерчанта
        summ = price - balance    #сумма которую нужно списать

        if card_id == None:
            print("no card")
            logger.error("У пользователя отсутсвует карта. Сессия с id: {}".format(session.id))
            # WARNING: Себе в логи и БД запсать, чтобы этот пользователь больше не заезжал по картк

        if summ > 0:
            #создаем платеж
            #parent_invoice = Invoice.objects.filter(profile=session.carnumber.profile, status='finish').latest('id')
            parent_invoice = session.carnumber.profile.parent_invoice
         
            if parent_invoice is None:
                print("Нет родительского платежа") #WARNING:

            data = {
                'profile' : session.carnumber.profile.id,
                'description' : "Пополнение счета на {} руб. за парковку".format(summ/100),
                'summ' : summ ,
                'parent_invoice' : parent_invoice.id,
                'parking_session_id' : session.id
            }
            serializer = ReceiptSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                invoice = serializer.save()

            make_payment(invoice)


def debiting_from_balance():
    """
    Данная функция списывает с баланса пользователя средства на оплату парковки
    """
    print(debiting_from_balance)

def check_balance_of_many_numbers(parking_client):
    logger.info("parking.services.check_balance_of_many_numbers. Проверяем сеансы парковок у API клиента: {}".format(parking_client))
    print("parking.services.check_balance_of_many_numbers ", type(parking_client))
    if type(parking_client) == int:
        parking_client = ParkingClient.objects.get(id=parking_client) # WARNING: Исключение если такой сессиии нет


    end_time =  timezone.now() - datetime.timedelta(minutes=45)
    #numbers = ParkingSession.objects.filter(parking_client=parking_client, finished=False, last_balance_check_time__lte=end_time)
    sessions = ParkingSession.objects.filter(parking_client=parking_client, payed=False, last_balance_check_time__lte=end_time)

    #print( (ParkingSession.objects.filter(parking_client=parking_client, finished=False, last_balance_check_time__lte=end_time).update(last_balance_check_time=timezone.now())).query)
    if sessions.count() != 0:
        print("count ", sessions.count())
        numbers = []
        for session in sessions:
            timedelta = 45

            if session.finished == True:
                timedelta = 0

            number = session.carnumber.number
            element = {"number" : number, "id" : session.id, "timedelta" : timedelta}
            if element not in numbers:
                numbers.append(element)
        sessions.update(last_balance_check_time=timezone.now())
        print(numbers)
        url = parking_client.get_price_request_url

        params = { "numbers" : numbers}

        try:
            response = requests.post(url, data=json.dumps(params))

            # если ответ успешен, исключения задействованы не будут
            response.raise_for_status()

            json_response = response.json()
            print(response)

            print(json_response)

            prices = json_response["prices"] # { "id" : Int, "price" : Int }

            for price_element in prices:
                session_id = price_element["id"]
                price = price_element["price"]

                #если это завершенная сессия, то нужно только списать с баланса. Если сессия не завершена, то пополнить баланс
                session = ParkingSession.objects.get(id=session_id)

                if session.finished == True:
                    debiting_from_balance(session)
                else:
                    schedule = Schedule.objects.create(
                            name="Check Balance",
                            func="parking.services.check_balance",
                            args=f"{session_id, price,}",
                        )
                    #t = multiprocessing.Process(target=check_balance, args=( session_id, price, ))
                    #t.start()




        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')  # Python 3.6
        except Exception as err:
            print(f'Other error occurred: {err}')  # Python 3.6
        else:
            print('Success! Got prices')

        #делаем запрос на получение цен
        #serializer = CarNumberFoAPIClientSerializer(numbers, many=True)
        #serializer = ListSerializer(numbers, many=True)
        #print(serializer.data)



'''
def check_and_make_autobalance_router(profile_id):
    profile = Profile.objects.get(id=profile_id)
    profile.check_and_make_autobalance()
'''
