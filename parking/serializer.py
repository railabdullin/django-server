from rest_framework import serializers
from .models import *

class ScaningObjectSerializer(serializers.ModelSerializer):
  class Meta:
    model = ParkingSession
    fields = '__all__'

class ScaningObjectListSerializer(serializers.ModelSerializer):
  class Meta:
    model = ParkingSession
    fields = '__all__'


class ScaningObjectCanceledSerializer(serializers.ModelSerializer):
  class Meta:
    model = ParkingSession
    fields = ('canceled',)

class ConfigurationSerializer(serializers.ModelSerializer):
  class Meta:
    model = ParkingClient
    fields = ('scanings_interval','send_prices_interval', 'price_time_delta')
