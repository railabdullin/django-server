from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone
from carnumbers.models import *
from connects.models import *
from django.contrib.auth.models import User
from django_q.models import Schedule


class ParkingClient(models.Model):
    user = models.ForeignKey(User, blank = True, null=True, on_delete=models.CASCADE) #нужен чтобы их api могло авторизоваться
    schedule = models.ForeignKey(Schedule, blank = True, null=True, on_delete=models.SET_NULL)
    name = models.CharField("Наименование организации" , max_length=100, blank = True, null=True)
    ogrn = models.CharField("ОГРН" , max_length=50, blank = True, null=True)
    legal_address = models.CharField("Юр. адрес" , max_length=300, blank = True, null=True)
    time = models.DateTimeField('Дата добавления', default=timezone.now)

    api_token = models.CharField("API токен" , max_length=50, blank = True, null=True, unique=True)
    get_price_request_url = models.CharField("Ссылка, куда отправляется GET запрос для получения стоимости за парковку" , max_length=1000, blank = True, null=True)

    type_choice = [('request', ('Делать запрос на URL клиента API (Обязательно указать URL)')), ('calculate', ('Определяем сами по тарифу')), ('recieve', ('Принимае запрос с ценами'))]
    get_price_method = models.CharField("Способ определения стоимости за парковку" ,choices = type_choice, default='request', max_length=100, blank = True, null=True)
    test_mode = models.BooleanField('Включен тестовый режим (не производится оплата)', default=False)

    send_prices_interval = models.IntegerField("Интервал между запросами на отправку цен в секундах" , blank = True, null=True, default=600)
    scanings_interval = models.IntegerField("Интервал между сканированиями при выезде, после неуспешной первой попытки выезда (в секундах)" , blank = True, null=True, default=30)
    price_time_delta = models.IntegerField("Время, которое добавляется к текущему сеанус парковки для расчета цены во время циклических запросов с ценами (в минутах) " , blank = True, null=True, default=40)

    def __str__(self):
        return str(self.name)

class ParkingSession(models.Model):
    parking_client = models.ForeignKey(ParkingClient , blank = True, null=True, on_delete=models.SET_NULL)
    carnumber = models.ForeignKey(CarNumber , blank = True, null=True, on_delete=models.SET_NULL)
    connect = models.ForeignKey("connects.Connect", related_name="rel_connect", blank = True, null=True, on_delete=models.SET_NULL)
    profile = models.ForeignKey(Profile , blank = True, null=True, on_delete=models.SET_NULL)
    #connect_id =  models.IntegerField(blank = True, null=True)
    begin_time = models.DateTimeField('Дата въезда', default=timezone.now)
    finish_time = models.DateTimeField('Дата выезда', blank = True, null=True, default=None)
    last_balance_check_time = models.DateTimeField('Время последней проверки стоимости', blank = True, null=True, default=timezone.now)
    parking_id = models.IntegerField("ID парковки" , blank = True, null=True, default=None)
    parking_address = models.CharField("Адрес парковки" , max_length=700, blank = True, null=True)

    parking_latitude = models.FloatField("Местоположение парковки широта" ,blank = True, null=True )
    parking_longitude = models.FloatField("Местоположение парковки долгота" ,  blank = True, null=True)

    rate_json = models.TextField("JSON с тарифами парковки" ,  blank = True, null=True)
    receipt_url = models.CharField("Ссылка на фотографию чека" , max_length=1000, blank = True, null=True)
    total_price = models.IntegerField("Итоговая стоимость парковки после завершения" , blank = True, null=True, default=0)

    is_come_in = models.BooleanField('True если это въезд', default=False)
    canceled = models.BooleanField('Отменено', default=False)
    finished = models.BooleanField('Завершено', default=False)
    payed = models.BooleanField('Оплачено', default=False)
    has_payment_error = models.BooleanField('Сеанс имеется проблемы с оплатой. Не выпустят из парковки', default=False)
    reccurent_schedules = models.ManyToManyField(Schedule,  blank=True)
    schedule_id = models.IntegerField(default=0)
    has_scheduler_for_reccurent = models.BooleanField('Есть очередь для проведения реккурентных платежей', default=False)
    active_reccurents_summ = models.IntegerField("Сумма активных реккурентных платежей в очереди (в копейках)" , blank = True, null=True, default=0)

    def __str__(self):
        return str(self.id) #WARNING: Не менять, так как Django Q берет id отсюда
