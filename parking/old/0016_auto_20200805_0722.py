# Generated by Django 3.0.6 on 2020-08-05 07:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('parking', '0015_parkingclient_price_time_deltal'),
    ]

    operations = [
        migrations.RenameField(
            model_name='parkingclient',
            old_name='price_time_deltal',
            new_name='price_time_delta',
        ),
    ]
