# Generated by Django 2.2.6 on 2020-05-21 16:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('connects', '0005_connect_title'),
        ('parking', '0005_auto_20200521_1618'),
    ]

    operations = [
        migrations.AddField(
            model_name='scaningobject',
            name='connect',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='connects.Connect'),
        ),
    ]
