# Generated by Django 3.0.6 on 2020-06-10 08:00

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('parking', '0010_parkingclient_schedule'),
    ]

    operations = [
        migrations.AddField(
            model_name='parkingsession',
            name='last_balance_check_time',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Время последней проверки стоимости'),
        ),
    ]
