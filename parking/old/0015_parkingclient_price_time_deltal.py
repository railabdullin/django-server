# Generated by Django 3.0.6 on 2020-08-05 07:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parking', '0014_auto_20200730_0738'),
    ]

    operations = [
        migrations.AddField(
            model_name='parkingclient',
            name='price_time_deltal',
            field=models.IntegerField(blank=True, default=40, null=True, verbose_name='Время, которое добавляется к текущему сеанус парковки для расчета цены во время циклических запросов с ценами (в минутах) '),
        ),
    ]
