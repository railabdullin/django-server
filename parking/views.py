from django.shortcuts import render
from rest_framework import generics
from .serializer import *
from .models import *
from carnumbers.models import *
from connects.serializer import *
from connects.models import *
from profiles.serializer import *
from rest_framework.response import Response
from django.utils import timezone
from rest_framework.generics import get_object_or_404
from django_q.models import Schedule #для асинхронных процессов
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.renderers import AdminRenderer
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
import json
from django.utils import timezone

from pyfcm import FCMNotification
push_service = FCMNotification(api_key="AAAAgyI-P1E:APA91bFF4P48_UKZXjD15mkIJR2fCEjTZJauGA7POu8-ckWCLvi89Az90gY_tJJi4m6q6zOCa5ktmz9W_8qBcQbu75pK4W6X9bsfJJCyq7NZk6CfBho9ONwY53wyu5m2Hho-uQb908xv")

import logging
logger = logging.getLogger(__name__)


class ScaningCreateView(generics.CreateAPIView):
    serializer_class = ScaningObjectSerializer

    def post(self, request):
        number = request.POST.get('number', None)
        target = request.POST.get('is_come_in', None)

        logger.info("Принят запрос на въезд/выезд. ГНЗ: {}, направление: {}, API клиент: {}".format(number, target, request.user))

        if number is None:
            logger.info("Отсутствует ГНЗ, код ошибки: 2")
            return Response({"message": "Bad request. Unexpected required params", "error": 2}, status=400)

        #проверяем есть ли в нашей базе такой номер
        numbers_count = CarNumber.objects.filter(number=number).count()
        print("numbers_count: ", numbers_count)
        if numbers_count == 0 :
            logger.info("Данный номер отсутсвует в базе Numpass")
            return Response({"error": 1}, status=200)
        elif numbers_count > 1:
            carnumber = CarNumber.objects.filter(number=number)[:1][0]
        else:
            carnumber = CarNumber.objects.get(number=number)


        #проверяем привязана ли банковская карта у пользователя
        profile = carnumber.profile
        if profile.parent_invoice == None:
            logger.info("У данного пользовтеля не привязана банковская карта")
            return Response({"error": 1}, status=200)

        #определяем клиента api
        user = request.user

        users_count = ParkingClient.objects.filter(user=user).count()
        print("users_count",users_count)
        if users_count == 0 :
            logger.info("Не сущетсвует API клиент с таким User")
            return Response({"error": 3}, status=200)
        elif users_count > 1:
            parking_client = ParkingClient.objects.filter(user=user)[:1][0]
            parking_client_profile = Profile.objects.filter(user=user)[:1][0]
        else:
            parking_client = ParkingClient.objects.get(user=user)
            parking_client_profile = Profile.objects.get(user=user)

        data = request.data.copy()
        data['carnumber'] = carnumber.id
        data['parking_client'] = parking_client.id
        logger.info("data: {}".format(data))
        response = {}

        time_now = timezone.localtime( timezone.now() ).strftime('%H:%M:%S')

        if "is_come_in" in data:
            if data['is_come_in'] == "1":
                prices_json_object = None
                if "rate_json" in data:
                    display_prices =  data['rate_json']
                    try:
                        prices_json_object = json.loads(display_prices)
                    except ValueError as e:
                        print("json error. data: ", display_prices)
           
                logger.info("создание сессии")
                #создаем сессию
                data['profile_id'] = profile.id 
                scaning_serializer = ScaningObjectSerializer(data=data)
                if scaning_serializer.is_valid(raise_exception=True):
                    saved_object = scaning_serializer.save()
                logger.info("создана сессия с id: {}".format(saved_object.id))

                logger.info("создание коннекта")
                #создаем коннект
                connect_data = {}
                connect_data['reciever_carnumber'] = carnumber.id
                connect_data['type'] = 'parking'

                parking_adress = request.POST.get('parking_address', None)

                if parking_adress is None:
                    parking_adress = parking_client.legal_address

                connect_data['title'] = parking_adress
                connect_data['parking_session'] = saved_object.id
             
                #print(display_prices)

                #parking_adress = request.POST.get('parking_address', None)


                serializer = ConnectForParkingDetailSerializer(data=connect_data)
                if serializer.is_valid(raise_exception=True):
                    new_connect = serializer.save()
                logger.info("Создан коннект с id: {}".format(new_connect.id))

                saved_object.connect = new_connect
                saved_object.save()
                #создаем сообщение c широтой и долготой
                new_message = Message()
                new_message.connect = new_connect
                new_message.reciever = carnumber.profile
                new_message.author = parking_client_profile
                new_message.type = "text"
                new_message.text = "Въезд на парковку {}".format(parking_adress)
                new_message.save()

                new_message = Message()
                new_message.connect = new_connect
                new_message.reciever = carnumber.profile
                new_message.author = parking_client_profile
                new_message.type = "text"
                new_message.text = "Время въезда {}".format(time_now)
                new_message.save()

                new_message = Message()
                new_message.connect = new_connect
                new_message.reciever = carnumber.profile
                new_message.author = parking_client_profile
                new_message.type = "text"
                new_message.text = "Тариф:"
                new_message.save()
                


                #создаем сообщения для тарифов
                if prices_json_object is not None:
                    for item in prices_json_object:
                        new_message = Message()
                        new_message.connect = new_connect
                        new_message.reciever = carnumber.profile
                        new_message.author = parking_client_profile
                        new_message.left_text = item['display_time']
                        new_message.right_text = item['display_price']
                        new_message.type = "price"
                        new_message.save()

                response = {"id" : saved_object.id}
 
                # TODO: Вывести все пуши в helpers
                message_title = saved_object.carnumber.number
                message_body = f'Въезд на объект: {parking_adress} '
                fcm_id = saved_object.carnumber.profile.notification_device_token
                logger.info("Попытка отправки Push уведомлений. fcm_id:: {}".format(fcm_id))
                if fcm_id != None:
                    result = push_service.notify_single_device(registration_id=fcm_id, message_title=message_title, message_body=message_body, sound="Default", data_message={"connect_id" : saved_object.connect.id})
                    logger.info("Ответ на запрос об отправке уведомлений: {}".format(result))

                # передаем в цикл проверки баланса
                #tasks.add.apply_async(queue='high_priority', args=()) #это с помощью celery
                # Create the schedule
                #schedule = Schedule.objects.create(
                #   name=self.__str__(),
                #   func="parking.services.check_balance",
                #   args=f"'{saved_object}'",
                #   schedule_type=Schedule.MINUTES,
                #)
                # Save the model with the schedule id
                #saved_object.schedule_id = schedule.pk
                #saved_object.save()

            else:
                price = request.POST.get('price', None)

                #if price is None:
                #    #response = {"status": "success" }
                price = int(price)
                #находим сеанс парковки, который соответсвует данному гнз и считаесят не завершенным. Также проверяем id парковки если он отправлен
                count = ParkingSession.objects.filter(carnumber=carnumber, finished=False).count() #возможно не нужно фильровать по finished так как с фильтрацией завершается не самый последний сеанса а последний активный 
                logger.info("Количество подходящих сессий: {}".format(count))
                if count == 0 :
                    logger.info("Не найдена сессия а таким ГНЗ")
                    return Response({"error": 4}, status=200)
                elif count > 1:
                    session = ParkingSession.objects.filter(carnumber=carnumber, finished=False).order_by('-id')[:1][0]
                else:
                    session = ParkingSession.objects.get(carnumber=carnumber, finished=False)

                

                #если сессия уже завершена и пытаются еще раз щавершить, то не нужно менять статус и время выезда
                
                logger.info("Найдена сессия с id: {}".format(session.id))
                #if session.has_payment_error == True or (session.carnumber.profile.cash <= 0 and price > 0):
                if True:
                    logger.info("В данной сессии были проблемы со списанием средств")

                    #смотрим баланс пользователя, если он меньше чем сума за парковку, то пытаемся списать средства
                    print("profile.cash ", session.carnumber.profile.cash)
                    print("price: ", price)
                    logger.info("profile.cash {}".format(session.carnumber.profile.cash))
                    logger.info("price: {}".format( price))
 
                    if session.carnumber.profile.cash < price:
                        summ = price - session.carnumber.profile.cash
                        print("summ: ", summ)
                        if summ <= 0:
                            session.finish_time = timezone.now()
                            session.finished = True
                            session.total_price = price
                            session.save()
                            new_message = Message()
                            new_message.connect = session.connect
                            new_message.reciever = carnumber.profile
                            new_message.author = parking_client_profile
                            new_message.type = "text"
                            new_message.text = "Выезд из парковки: {}. Стоимость парковки: {} руб.".format(time_now, price/100)
                            new_message.save()
                            
                            response = {"status": "success" }
                        else:
                            logger.info("Баланс меньше стоимости сеанса парковки. Выезд запрещен. Баланс: {}, стоимость сеанса парковки: {}".format(session.carnumber.profile.cash, price))
                            #показывает push уведомелние что не выедет пока не оплатит
                            message_title = session.carnumber.number
                            message_body = "У вас есть долг. Возникли проблемы при оплате с банковской картой. Проверьте баланс"
                            fcm_id = session.carnumber.profile.notification_device_token
                            print("Попытка отправки Push уведомлений. fcm_id:: {}".format(fcm_id))
                            logger.info("Попытка отправки Push уведомлений. fcm_id:: {}".format(fcm_id))
                            if fcm_id != None:
                                result = push_service.notify_single_device(registration_id=fcm_id, message_title=message_title, message_body=message_body, sound="Default", data_message={"connect_id" : session.connect.id})
                                print (result)
                                logger.info("Ответ на запрос об отправке уведомлений: {}".format(result))
                            print("попытка списания средств")
                            logger.info("попытка списания средств")
                            #попытка списания средств
                            #сервер не дает ответа пока не получит информацию об оплате
                            #WARNING: Сделать попытку реккурентного платежа
                            parent_invoice = session.carnumber.profile.parent_invoice    
                            data = {
                       'profile' : session.carnumber.profile.id,
                'description' : "Списание стоимости парковки при выезде из-за мелого баланса {} руб.".format(summ/100),
                'summ' : summ ,
                'parent_invoice' : parent_invoice.id,
                'parking_session_id' : session.id
                            }
                            serializer = ReceiptSerializer(data=data)
                            if serializer.is_valid(raise_exception=True):
                                invoice = serializer.save()
                            logger.info("Создан invoice {}".format(invoice))
                            '''
                            #если уже есть Scheduler на оплату и сумма в ней совпадает с суммой, которую сейчас хотим создать, то не создаем новый
                            # если новая сумма для реккурентного платежа больше чем сумма всех предыдущих, то создаем очередь на оплату тольку на эту разницу
                            new_reccurent_summ = 0
                            if session.has_scheduler_for_reccurent:
                                if price > session.active_reccurents_summ:
                                    new_reccurent_summ = price - session.active_reccurents_summ
                            else:
                                new_reccurent_summ = price
                            logger.info("new_reccurent_summ: ", new_reccurent_summ)

                            if new_reccurent_summ > 0:
                                session.has_scheduler_for_reccurent = True
                                session.active_reccurents_summ = session.active_reccurents_summ + new_reccurent_summ
                                session.save()
                                schedule = Schedule.objects.create(
                                    name="check_balance",
                                    func="parking.services.check_balance",
                                    args=f"{session.id, new_reccurent_summ,}",
                                )
                            '''
                            # удаляем все schedule на оплату и создаем списание
                            for old_schedule in session.reccurent_schedules.all():
                                old_schedule.delete()

                            schedule = Schedule.objects.create(
                                name="Make Payment (Recurrent) on exit from parking",
                                func="parking.services.make_payment",
                                args=f"{invoice.id,}",
                            )
                            logger.info("Создан schedule для make payment invoice.id {}".format(invoice.id))


                            response = {"status": "pay_error", "error" : 5 }
                    else:
                        #списание с баланса
                        #warning: вывести в отедльную функцию и отправить чек
                        logger.info("Списание с баланса. Выезд разрешен")
                        profile = session.carnumber.profile
                        profile.cash -= price
                        profile.save()
                        session.finish_time = timezone.now()
                        session.finished = True
                        session.total_price = price
                        session.save()

                        new_message = Message()
                        new_message.connect = session.connect
                        new_message.reciever = carnumber.profile
                        new_message.author = parking_client_profile
                        new_message.type = "text"
                        new_message.text = "Выезд из парковки: {}. Стоимость парковки: {} руб.".format(time_now, price/100)
                        new_message.save()

                        logger.info("С баланса списано {} р. ".format(price/100))
                        
                        message_title = session.carnumber.number
                        message_body = "Выезд с объекта. Платеж {} р.".format(price/100)
                        fcm_id = session.carnumber.profile.notification_device_token
                        logger.info("Попытка отправки Push уведомлений. fcm_id:: {}".format(fcm_id))
                        if fcm_id != None:
                            result = push_service.notify_single_device(registration_id=fcm_id, message_title=message_title, message_body=message_body, sound="Default")
                            
                            logger.info("Ответ на запрос об отправке уведомлений: {}".format(result))
                        
                        response = {"status": "success" }

                else:
                    if session.finished == False:
                        session.finish_time = timezone.now()
                        session.finished = True
                        session.total_price = price
                        
                        session.save()

                        new_message = Message()
                        new_message.connect = session.connect
                        new_message.reciever = carnumber.profile
                        new_message.author = parking_client_profile
                        new_message.type = "text"
                        new_message.text = "Выезд с объекта. Платеж {} р.".format(time_now, price)
                        new_message.save()


                        profile = session.carnumber.profile
                        profile.cash -= price
                        profile.save()

                        logger.info("С баланса списано {} р. ".format(price/100))
                        message_title = session.carnumber.number
                        message_body = "Выезд с объекта. Платеж {} р.".format(price/100)
                        fcm_id = session.carnumber.profile.notification_device_token
                        logger.info("Попытка отправки Push уведомлений. fcm_id: {}".format(fcm_id))
                        if fcm_id != None:
                            result = push_service.notify_single_device(registration_id=fcm_id, message_title=message_title, message_body=message_body)
                            logger.info("Ответ на запрос об отправке уведомлений: {}".format(result))
                        #формируем сообщение в конект
                    logger.info("Проблем с пополнением баланса не было. Выезд разрешен") 
                    response = {"status": "success" }


                #после этого этот сеанс парковки будет обработан Django Q, чтобы списать с баланса средства. смотри services.check_balance_of_many_numbers
        logger.info("Respone: {}".format(response))
        print(profile)
        schedule = Schedule.objects.create(
                                name="profile check and make autobalance",
                                func="profiles.services.check_and_make_autobalance_router",
                                args=f"{profile.id}",
                            ) 
        #profile.check_and_make_autobalance()
        return Response(response)



class ListView(generics.ListAPIView):
    serializer_class = ScaningObjectListSerializer
    queryset = ParkingSession.objects.all()

class CanceledUpdateView(generics.UpdateAPIView):
    serializer_class = ScaningObjectCanceledSerializer
    queryset = ParkingSession.objects.all()

    def put(self, request, pk):
        object = get_object_or_404(ParkingSession.objects.all(),pk=pk)

        connect = object.connect.get()
        print(connect)


        if object.canceled == False and request.POST.get('canceled','0') == '1':
            #создаем сообщение об отмене
            new_message = Message()
            new_message.connect = connect
            new_message.reciever = connect.reciever_carnumber.profile
            #new_message.author = parking_client_profile
            new_message.text = "Отменено"
            new_message.type = "text"
            new_message.save()

        serializer = ScaningObjectCanceledSerializer(instance=object, data=request.data, partial=True)
        if serializer.is_valid(raise_exception=True):
            me_saved = serializer.save()
        return Response(serializer.data)



class DeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ScaningObjectSerializer
    queryset = ParkingSession.objects.all()


class SessionsListView(LoginRequiredMixin, ListView):
    model = ParkingSession
    template_name ='parking/table.html'
    renderer_classes = [AdminRenderer, ]
    login_url = '/accounts/login/'
    
    def get_queryset(self):
        user = self.request.user
        if user.has_perm('parking.view_parkingsession'):
            queryset = ParkingSession.objects.all().order_by('-id')
        else:
            parking_client = get_object_or_404(ParkingClient, user=user)
            queryset = ParkingSession.objects.filter(parking_client=parking_client).order_by('-id')
        return queryset
    

class PricesForSessionsHandler(APIView):
    def post(self, request, format=None):
        data = request.data.copy()
        logger.info("Получили массив цен от {} с содержимым: {}".format(request.user, data))
               

        prices = data["prices"] # { "id" : Int, "price" : Int }

        for price_element in prices:
            session_id = price_element["id"]
            price = price_element["price"]

            #Если сессия не завершена, то пополнить баланс
            count = ParkingSession.objects.filter(id=session_id).count()
            if count == 0:
                continue

            session = ParkingSession.objects.get(id=session_id)

            #если сессия не принадлежит этому api клиенту то не делаем оплату
            if session.parking_client.user != request.user:
                continue

            if session.finished:
                continue

            #если сеанс считается отмененным, то не списывает с баланса
            if session.canceled:
                continue

            #если уже есть Scheduler на оплату и сумма в ней совпадает с суммой, которую сейчас хотим создать, то не создаем новый
            # если новая сумма для реккурентного платежа больше чем сумма всех предыдущих, то создаем очередь на оплату тольку на эту разницу
            new_reccurent_summ = 0
            if session.has_scheduler_for_reccurent:
                if price > session.active_reccurents_summ:
                    new_reccurent_summ = price - session.active_reccurents_summ
            else:
                new_reccurent_summ = price

            if new_reccurent_summ > 0:
                session.has_scheduler_for_reccurent = True
                session.active_reccurents_summ = session.active_reccurents_summ + new_reccurent_summ
                schedule = Schedule.objects.create(
                            name="check_balance",
                            func="parking.services.check_balance",
                            args=f"{session_id, new_reccurent_summ,}",
                        )
                session.reccurent_schedules.add(schedule)
                session.save()
        return Response({"message": "ok", "error" : 0 })


class ConfigurationView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ConfigurationSerializer
    
    def get_object(self):
        user = self.request.user
        parking_client = ParkingClient.objects.get(user=user)
        return parking_client
