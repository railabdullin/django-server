from django.urls import path
from .views import *

urlpatterns = [
  path('api/parking/scanings/create', ScaningCreateView.as_view()),
  path('api/parking/scanings/', ListView.as_view()), # WARNING: удалить нужно
  path('api/parking/scanings/<int:pk>', DeleteView.as_view()),
  path('api/parking/scanings/<int:pk>/canceled_status', CanceledUpdateView.as_view()),
  path('api/parking/prices/', PricesForSessionsHandler.as_view()),
  path('api/parking/configurations/', ConfigurationView.as_view()),

  path('lk/scanings/', SessionsListView.as_view()),
]
