from rest_framework import serializers
from .models import *
from carnumbers.serializer import *
from parking.serializer import *

class ListSerializer(serializers.ModelSerializer):
    author_carnumber = CarNumberDetailSerializer()
    reciever_carnumber = CarNumberDetailSerializer()
    parking_session = ScaningObjectSerializer()
    class Meta:
        model = Connect
        fields = '__all__'

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


'''
#используется только при отправке стикера
class MessageForSendingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'time', 'reciever', 'author', 'text', 'recieved', 'location_longitude', 'location_lattitude', 'sticker_id')
'''

class PersonConnectDetailSerializer(serializers.ModelSerializer):
    messages = serializers.SerializerMethodField()
    author_carnumber = CarNumberDetailSerializer()
    reciever_carnumber = CarNumberDetailSerializer()

    class Meta:
        model = Connect
        fields = ('id',  'type',  'author_carnumber', 'reciever_carnumber', 'messages')

    def get_messages(self, object):
        messages = Message.objects.filter(connect=object).order_by('id')
        serializer = MessageSerializer(messages, many=True)
        return serializer.data

class ParkingConnectDetailSerializer(serializers.ModelSerializer):
    messages = serializers.SerializerMethodField()
    reciever_carnumber = CarNumberDetailSerializer()
    parking_session = ScaningObjectSerializer()

    class Meta:
        model = Connect
        fields = ('id',  'type', 'reciever_carnumber','parking_session', 'messages')

    def get_messages(self, object):
        messages = Message.objects.filter(connect=object).order_by('id')
        serializer = MessageSerializer(messages, many=True)
        return serializer.data

''' При создании коннекта при парковке '''
class ConnectForParkingDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Connect
        fields = ('id', 'type', 'title', 'reciever_carnumber','parking_session')
