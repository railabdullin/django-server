from django.contrib import admin

# Register your models here.
from .models import *

class ConnectAdmin(admin.ModelAdmin):
    list_display = ('title', 'author_carnumber', 'reciever_carnumber', 'type', 'start_time', 'finished', 'parking_session')

class MessageAdmin(admin.ModelAdmin):
    list_display = ('title', 'author_carnumber', 'reciever_carnumber', 'type', 'start_time', 'finished', 'parking_session')

admin.site.register(Connect, ConnectAdmin)
admin.site.register(Message)
