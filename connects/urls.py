from django.urls import path
from .views import *

urlpatterns = [
  path('connects/create/', NewConnectCreateView.as_view()),
  path('connects/', ListView.as_view()),
  path('connects/<int:pk>', DeleteView.as_view()),
  path('connects/<int:pk>/send/', SendStickerView.as_view()),
]
