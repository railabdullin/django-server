# Generated by Django 2.2.6 on 2020-05-26 08:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('parking', '0007_auto_20200526_0854'),
        ('connects', '0005_connect_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='connect',
            name='parking_session',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='parking_session', to='parking.ParkingSession'),
        ),
    ]
