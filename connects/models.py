from django.db import models
from django.utils import timezone
from carnumbers.models import *
from profiles.models import *
from parking.models import *




# Create your models here.
class Connect(models.Model):
  author_carnumber = models.ForeignKey(CarNumber, related_name='author_car_number' , blank = True, null=True, on_delete=models.SET_NULL)
  reciever_carnumber = models.ForeignKey(CarNumber, related_name='reciever_car_number' , blank = True, null=True,  on_delete=models.SET_NULL)
  author_profile = models.ForeignKey(Profile, related_name='connect_author_profile' , blank = True, null=True,  on_delete=models.SET_NULL)
  parking_session = models.ForeignKey(ParkingSession, related_name='rel_connect' , blank = True, null=True,  on_delete=models.SET_NULL)


  start_time = models.DateTimeField('Дата добавления', default=timezone.now)
  finished = models.BooleanField('Коннект завершен', default=False)

  type_choice = [('parking', ('По поводу парковки')), ('person', ('Личный коннект')),]
  type = models.CharField("Тип коннекта" ,choices = type_choice, default='person', max_length=100, blank = True, null=True)
  title = models.CharField("Заголовок коннекта", max_length=100, blank = True, null=True)


  #location_longitude = models.FloatField("Долгота" ,max_length=100, blank = True, null=True)
  #location_lattitude = models.FloatField("Широта" ,max_length=100, blank = True, null=True)

  def __str__(self):
    return "Автор {}, получатель {}".format(self.author_carnumber, self.reciever_carnumber)

class Message(models.Model):
  connect = models.ForeignKey(Connect , blank = True, null=True, on_delete=models.SET_NULL)

  reciever = models.ForeignKey(Profile, related_name='_reciever' , blank = True, null=True, on_delete=models.SET_NULL)
  author = models.ForeignKey(Profile, related_name='_author' , blank = True, null=True, on_delete=models.SET_NULL)

  time = models.DateTimeField('Время сообщения', default=timezone.now)
  text = models.CharField("Текст сообщения" ,  max_length=1000, blank = True, null=True)
  image_link = models.CharField("Ссылка на изображение" ,  max_length=1000, blank = True, null=True)
  left_text = models.CharField("Левый столбец ячейки цены" ,  max_length=1000, blank = True, null=True)
  right_text = models.CharField("Правый столбец ячейки цены" ,  max_length=1000, blank = True, null=True)
  #status = models.CharField("Статус" ,max_length=100, blank = True, null=True)

  #sended = models.BooleanField('Сообщений отправлено', default=False)
  recieved = models.BooleanField('Сообщение получено', default=False)

  location_longitude = models.FloatField("Долгота" , blank = True, null=True)
  location_lattitude = models.FloatField("Широта" , blank = True, null=True)

  sticker_id = models.IntegerField("ID стикера" ,default=0, blank = True, null=True)
  type_choice = [('map', ('Карта')), ('sticker', ('Стикер')), ('price', ('Тариф')), ('image', ('Изображение')), ('text', ('Текст'))]
  type = models.CharField("Тип сообщения" ,choices = type_choice, default='sticker', max_length=100, blank = True, null=True)


  def __str__(self):
    if self.connect is not None:
        return "{} для коннекта {}".format(self.type, self.connect.id)
    else:
        return "{}. Без коннекта".format(self.type)
