from django.shortcuts import render
from rest_framework import generics
from .serializer import *
from .models import *
from carnumbers.models import *
from profiles.models import *
from blacklist.models import *
from rest_framework.response import Response
#import datetime
from rest_framework.generics import get_object_or_404
from datetime import datetime, timedelta
from django.core.exceptions import ObjectDoesNotExist

#ДЛЯ push уведомлений
from pyfcm import FCMNotification
push_service = FCMNotification(api_key="AAAAgyI-P1E:APA91bFF4P48_UKZXjD15mkIJR2fCEjTZJauGA7POu8-ckWCLvi89Az90gY_tJJi4m6q6zOCa5ktmz9W_8qBcQbu75pK4W6X9bsfJJCyq7NZk6CfBho9ONwY53wyu5m2Hho-uQb908xv")


# Create your views here.
class NewConnectCreateView(generics.CreateAPIView):
    serializer_class = PersonConnectDetailSerializer

    def post(self, request):
        carnumber_id = request.POST.get('carnumber_id', None)
        location_longitude = request.POST.get('longitude', None)
        location_lattitude = request.POST.get('lattitude', None)
        sticker_id = request.POST.get('sticker_id', None)

        print(carnumber_id, location_longitude, location_lattitude, sticker_id)

        if carnumber_id is not None and location_longitude is not None and location_longitude is not None and sticker_id is not None:
            if CarNumber.objects.filter(id = carnumber_id).exists():
                reciever_carnumber = CarNumber.objects.get(id = carnumber_id)
                reciever = reciever_carnumber.profile
                author = Profile.objects.get(user = request.user )

                #получаем приоритетное авто у отправителя
                main_author_carnumber_count = CarNumber.objects.filter(profile=author, is_main_transport=True).count()
                if main_author_carnumber_count == 0:
                    all_author_carnumber_count = CarNumber.objects.filter(profile=author).count()
                    if all_author_carnumber_count == 0:
                        return Response({"message": "You have not car."}, status=256)
                    else:
                        author_carnumber = CarNumber.objects.filter(profile=author)[:1][0]
                else:
                    author_carnumber = CarNumber.objects.filter(profile=author, is_main_transport=True)[:1][0]


                #проверить нет ли в чс у этого пользователя
                in_blacklist_count = Blacklist.objects.filter(author_profile=reciever, blocked_profile=author ).count()

                if in_blacklist_count > 0:
                    return Response({"message": "You are blocked by this user"}, status=257)

                '''
                #проверяем не отправляли ли ему стикеры в ближайшие 2 часа
                start_time_filter= datetime.now() - timedelta(hours=2)
                last_connects_count = Connect.objects.filter(author_carnumber=author_carnumber, reciever_carnumber=reciever_carnumber, start_time__gte=start_time_filter ).count()
                if last_connects_count > 0:
                    return Response({"message": "You sended message to this user in last 2 hours"}, status=258)
                '''

                #проверяем нет ли у него активных коннектов
                time_threshold = datetime.now() - timedelta(hours=2)
                count = Connect.objects.filter(author_profile=author, start_time__gte=time_threshold, finished=False).count()
                if count > 0:
                    return Response({"message": "You have active connect"}, status=259)

                new_connect = Connect()
                new_connect.author_carnumber = author_carnumber
                new_connect.author_profile = author
                new_connect.reciever_carnumber = reciever_carnumber
                new_connect.title = author_carnumber.number
                new_connect.save()

                #создаем сообщение c широтой и долготой
                new_message = Message()
                new_message.connect = new_connect
                new_message.reciever = reciever
                new_message.author = author
                new_message.location_longitude = location_longitude
                new_message.location_lattitude = location_lattitude
                new_message.type = "map"
                new_message.save()

                new_message = Message()
                new_message.sticker_id = sticker_id
                new_message.connect = new_connect
                new_message.reciever = reciever
                new_message.author = author
                new_message.save()


                serializer = PersonConnectDetailSerializer(new_connect, many=False)
                if reciever_carnumber.profile.notification_device_token is not None:
                    notification_id = [reciever_carnumber.profile.notification_device_token, ] 
                    print("notification_id ", notification_id)
                    if notification_id is not None:
                        message_title = author_carnumber.number
                        message_body = get_stcker_name(sticker_id)
                        result = push_service.notify_multiple_devices(registration_ids=notification_id, message_title=message_title, message_body=message_body, sound="Default", data_message={"connect_id" : new_connect.id})
                        print (result)
                return Response(serializer.data)

            else:
                return Response({"message": "No Content. Cur number not exists"}, status=204)
        else:
            return Response({"message": "Bad request. Unexpected required params"}, status=400)

class ListView(generics.ListAPIView):
    serializer_class = ListSerializer
    queryset = Connect.objects.all()

    def get(self, request, format=None, *args, **kwargs):
        user_profile = get_object_or_404(Profile.objects.all(),user=request.user)
        '''
        author_carnumber = models.ForeignKey(CarNumber, related_name='author_car_number' , blank = True, null=True, on_delete=models.SET_NULL)
        reciever_carnumber
        '''

        my_car_numbers = CarNumber.objects.filter(profile = user_profile)

        if len(my_car_numbers) != 0:
            instance = Connect.objects.filter(reciever_carnumber__in = my_car_numbers).order_by('-id')

            serializer = ListSerializer(instance, many=True)
        else:
            serializer = ListSerializer([], many=True)
        return Response(serializer.data)


class DeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PersonConnectDetailSerializer
    queryset = Connect.objects.all()

    def get(self, request, pk):
        user_profile = get_object_or_404(Profile.objects.all(),user=request.user)
        object = get_object_or_404(Connect.objects.all(), pk=pk)

        my_car_numbers = CarNumber.objects.filter(profile = user_profile)
        print("my_car_numbers: ", my_car_numbers)
        print("object.reciever_carnumber ", object.reciever_carnumber)
        title = None
        if object.reciever_carnumber in my_car_numbers or object.author_carnumber in my_car_numbers:
            #разделяем коннекты для парковок и личные коннекты
            if object.type == 'person':
                serializer = PersonConnectDetailSerializer(object, many=False)
                data = serializer.data.copy()

                #получаем стикеры, доступные для отправки
                if user_profile == object.author_carnumber.profile:
                    stickers_for_sending = get_stickers_for_sending(object, 'author')
                    title = object.reciever_carnumber.number
                else:
                    stickers_for_sending = get_stickers_for_sending(object, 'reciever')
                    title = object.author_carnumber.number

            elif object.type == 'parking':
                serializer = ParkingConnectDetailSerializer(object, many=False)
                data = serializer.data.copy()
                # смотрим завершен ли сеанс парковки. Если нет, то добавляем кнопку отмены
                stickers_for_sending = []
                if object.parking_session is not None:
                    session = object.parking_session
                    if not session.finished and not session.canceled:
                        stickers_for_sending.append(11)
            else:
                data = {}

            data['stickers_for_sending'] = stickers_for_sending
            if title is not None:
                data['title'] = title
            return Response(data)
        else:
            return Response({"message": "not rights"}, status=405)


def get_stickers_for_sending(connect, position):
    if connect is not None:

        #задаем список стикеров, которые могут быть отправлены, в заисимости от роли в коннекте

        '''
        0       кнопка завершить removed
        1       мешает на парковке removed
        2       эвакуируют removed
        3       авария removed
        4       отмена
        5       5 минут
        6       10 минут
        7       15 минут
        8       бегу
        9       заблокировать
        10      единственный стикер отправителя

        '''

        if position == 'author':
            available_stikers = [4]
        elif position == 'reciever':
            available_stikers = [5,6,7,8,9]
        else:
            available_stikers = []

        #получаем все сообщения из этого коннекта
        messages = Message.objects.filter(connect=connect)

        for message in messages:
            if message.sticker_id is not None:
                if message.sticker_id in available_stikers:
                    available_stikers.remove(message.sticker_id)

                if message.sticker_id == 4:
                    available_stikers.append(10)

                if message.sticker_id == 9:
                    available_stikers.append(10)

        return available_stikers

def get_stcker_name(sticker_id):
    stikers_names = ["Кнопка завершить","Вызов","Эвакуируют","Авария","Отмена","2 минуты","5 минут","15 минут","Бегу","Заблокировать","Вызов"]
    print("sticker_id ", sticker_id)
    if type(sticker_id) == str:
        sticker_id = int(sticker_id)
    return stikers_names[sticker_id]

class SendStickerView(generics.CreateAPIView):
    serializer_class = MessageSerializer

    def post(self, request, pk):
        if Connect.objects.filter(id = pk).exists():
            connect = Connect.objects.get(id = pk)

            author = Profile.objects.get(user = request.user )

            #получаем получателя сообщения и за одно получаем стикеры, которые можем отправить
            if author == connect.author_carnumber.profile:
                vehicle_for_blocking = connect.reciever_carnumber
                reciever = connect.reciever_carnumber.profile
                message_author_carnumber = connect.author_carnumber
                stickers_for_sending = get_stickers_for_sending(connect, 'author')
                notification_id = [reciever.notification_device_token]
            else:
                vehicle_for_blocking = connect.author_carnumber
                reciever = connect.author_carnumber.profile
                message_author_carnumber = connect.reciever_carnumber
                stickers_for_sending = get_stickers_for_sending(connect, 'reciever')
                print("reciver. token= ", connect.author_carnumber.profile)
                notification_id = [connect.author_carnumber.profile.notification_device_token]
            print(notification_id)
            #проверить нет ли в чс у этого пользователя 
            in_blacklist_count = Blacklist.objects.filter(author_profile=reciever, blocked_profile=author ).count()

            if in_blacklist_count > 0:
                return Response({"message": "You are blocked by this user"}, status=257)

            '''
            #проверяем не отправляли ли ему стикеры в ближайшие 2 часа
            start_time_filter= datetime.datetime.now() - datetime.timedelta(hours=2)
            last_connects_count = Connect.objects.filter(author_carnumber=author_carnumber, reciever_carnumber=reciever_carnumber, start_time__gte=start_time_filter ).count()
            if last_connects_count > 0:
                return Response({"message": "You sended message to this user in last 2 hours"}, status=408)
            '''

            data = request.data.copy()


            #проверяем может ли он отправлять этот стикер
            print(stickers_for_sending)
            sticker_id = int(data['sticker_id'])
            if not sticker_id in stickers_for_sending:
                return Response({"message": "You can't send this sticker"}, status=205)

            #удаляем стикер который сейчас отправляем из доступных для отправки
            stickers_for_sending.remove(sticker_id)

            #если sticker id == 9 то блокируем пользователя
            if sticker_id == 9:
                blacklist_object = Blacklist()
                blacklist_object.author_profile = author
                blacklist_object.blocked_profile = reciever
                blacklist_object.blocked_vehicle = vehicle_for_blocking
                blacklist_object.save()



            #проверяем не отменен ли чат
            if connect.finished:
                return Response({"message": "Connect finished"}, status=408)

            data['author'] = author.id
            data['reciever'] = reciever.id
            data['connect'] = connect.id

            serializer = MessageSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                ad_saved = serializer.save()


            #если sticker id == 4 то завершаем чат
            if sticker_id == 4 or sticker_id == 9:
                connect.finished = True
                connect.save()


            #если это не стикер блокировки, то отправляем push уведомление
            if sticker_id != 9 and notification_id is not None:
                message_title = message_author_carnumber.number
                message_body = get_stcker_name(sticker_id)
                result = push_service.notify_multiple_devices(registration_ids=notification_id, message_title=message_title, message_body=message_body, sound="Default", data_message={"connect_id" : connect.id})
                print (result)

            data = serializer.data.copy()
            data['stickers_for_sending'] = stickers_for_sending

            return Response(data)

        else:
            return Response({"message": "No Content. Cur number not exists"}, status=204)
