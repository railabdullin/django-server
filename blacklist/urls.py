from django.urls import path
from .views import *

urlpatterns = [
  path('blacklist/create/', CreateView.as_view()),
  path('blacklist/', ListView.as_view()),
  path('blacklist/<int:pk>', DeleteView.as_view()),
]
