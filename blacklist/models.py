from django.db import models
from django.utils import timezone
from carnumbers.models import *
from profiles.models import *

# Create your models here.
class Blacklist(models.Model):
    #author_carnumber = models.ForeignKey(CarNumber, related_name='carnumber'  , blank = True, null=True, on_delete=models.SET_NULL)
    author_profile = models.ForeignKey(Profile, related_name='_author_profile'  , blank = True, null=True, on_delete=models.SET_NULL)
    blocked_vehicle = models.ForeignKey(CarNumber, related_name='vehicle' , blank = True, null=True, on_delete=models.SET_NULL)
    blocked_profile = models.ForeignKey(Profile, related_name='_blocked_profile' , blank = True, null=True, on_delete=models.SET_NULL)
    time = models.DateTimeField('Дата добавления', default=timezone.now)
