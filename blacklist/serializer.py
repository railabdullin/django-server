from rest_framework import serializers
from .models import *
from carnumbers.serializer import *

class DetailSerializer(serializers.ModelSerializer):
  blocked_vehicle = CarNumberDetailSerializer()
  class Meta:
    model = Blacklist
    fields = '__all__'

class ListSerializer(serializers.ModelSerializer):
  blocked_vehicle = CarNumberDetailSerializer()
  class Meta:
    model = Blacklist
    fields = '__all__'
