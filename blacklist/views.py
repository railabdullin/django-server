from django.shortcuts import render
from rest_framework import generics
from .serializer import *
from .models import *
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from django.http import HttpResponse

from rest_framework.pagination import PageNumberPagination
from .pagination import PaginationHandlerMixin

class BasicPagination(PageNumberPagination):
    page_size_query_param = 'limit'

# Create your views here.
class CreateView(generics.CreateAPIView):
    serializer_class = DetailSerializer

class ListView(generics.ListAPIView):
    pagination_class = BasicPagination
    serializer_class = ListSerializer
    queryset = Blacklist.objects.all()

    def get(self, request, format=None, *args, **kwargs):
        user_profile = Profile.objects.get(user = request.user)

        instance = Blacklist.objects.filter(author_profile = user_profile).order_by('-id')
        page = self.paginate_queryset(instance)
        if page is not None:
            serializer = self.get_paginated_response(self.serializer_class(page, many=True, context={'request': request}).data)
        else:
            serializer = self.serializer_class(instance, many=True)
        return Response(serializer.data)

class DeleteView(generics.RetrieveUpdateDestroyAPIView):
  serializer_class = DetailSerializer
  queryset = Blacklist.objects.all()

  def delete(self, request, pk):
        # Get object with this pk
        item = get_object_or_404(Blacklist.objects.all(), pk=pk)
        user_profile = Profile.objects.get(user = self.request.user)

        if (item.author_profile == user_profile):
           item.delete()
           return HttpResponse(status=204)
        elif (item.author_profile != user_profile):
           return Response({"message": "No rights"}, status=405)
