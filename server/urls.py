from django.contrib import admin
from django.urls import path, include
from profiles.views import *
from knox import views as knox_views
from django.views.generic.base import TemplateView
import debug_toolbar

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('connects.urls')),
    path('api/', include('blacklist.urls')),
    path('api/', include('carnumbers.urls')),
    path('', include('parking.urls')),
    path('api/', include('profiles.urls')),
    path('api/payment/callback/', InvoicePaymentCallback.as_view()),

    path('api/logout/', knox_views.LogoutView.as_view(), name='knox_logout'),
    path('api/logoutall/', knox_views.LogoutAllView.as_view(), name='knox_logoutall'),

    path('accounts/', include('django.contrib.auth.urls')),
    path('api/privacy/', TemplateView.as_view(template_name="privacy.html") ),
    path('api/__debug__/', include(debug_toolbar.urls)),

]
